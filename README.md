## 1. Spawn object launch files
The following launch files are provided to spawn different types of objects:
* spawn\_sitting\_person.launch

### Options
Each launch file comes with a standard set of options.

**e.g.** `roslaunch lasr_teleop_object spawn_sitting_person.launch object_no:=0 model_no:=0 R:=1.5`

The full list of options is tabulated below:

| Argument | Default value | Description |
| ------ | ------ |  ------ |
| spawn_name | "spawn_model" | unique name for the gazebo_ros spawn_model topic |
| object_no | 0 | unique number for naming gazebo model and ROS topic |
| model_no | 0 | number selection for 3D model to render |
| x | 0 | Position (x) in metres |
| y | 0 | Position (y) in metres |
| z | 0 | Position (z) in metres |
| R | 0 | Rotation (roll) in radians |
| p | 0 | Rotation (pitch) in radians |
| Y | 0 | Rotation (yaw) in radians |

## 2. Keyboard Teleoperation
`src/key_teleop.py` uses **WASD** input to send Twist messages to the topic `/lasr_object_controller/<object name>/cmd_vel` 

<br>

**usage:** `rosrun lasr_teleop_object key_teleop.py <object_name> [speed]`

**e.g.** `rosrun lasr_teleop_object key_teleop.py person_0 2`

## 3. References

sitting people 3D models taken from https://gitlab.com/sensible-robots/custom_worlds by Logan Dunbar