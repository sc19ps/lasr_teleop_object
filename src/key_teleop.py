#!/usr/bin/python


import sys
import rospy
import curses
from geometry_msgs.msg import Twist, Vector3


def main(stdscr, object_name, speed):
    # key setup
    KEYS = {}
    KEYS['W']    = Twist(Vector3(0,-speed,0), Vector3(0,0,0))
    KEYS['A']    = Twist(Vector3(0,0,0)     , Vector3(0,0,speed))
    KEYS['S']    = Twist(Vector3(0,speed,0) , Vector3(0,0,0))
    KEYS['D']    = Twist(Vector3(0,0,0)     , Vector3(0,0,-speed))
    KEYS['STOP'] = Twist(Vector3(0,0,0)     , Vector3(0,0,0))

    # setup
    stdscr.timeout(10)
    key = ''
    pub = rospy.Publisher('/lasr_object_controller/{}/cmd_vel'.format(object_name), Twist)
    time_last = rospy.Time.now().secs
    
    try:
        while not rospy.is_shutdown():
            stdscr.clear()
            stdscr.addstr('{}\nTank controls: WASD to move, Q to quit\n'.format(object_name))

            # stop movement if no key press for 10ms
            if rospy.Time.now().secs - time_last > 0.1:
                pub.publish(KEYS['STOP'])
            else:
                stdscr.addstr(key)

            # refresh screen and flush input
            stdscr.refresh()
            curses.flushinp()

            # read key press
            try:
                key = stdscr.getkey().upper()
                if key in KEYS:
                    pub.publish(KEYS[key])
                    time_last = rospy.Time.now().secs
                elif key == 'Q':
                    break
            except curses.error:
                pass
    
    except KeyboardInterrupt:
        pass
    
    pub.publish(KEYS['STOP'])


if __name__ == '__main__':
    if not (len(sys.argv) == 2 or len(sys.argv) == 3):
        print 'usage: key_teleop.py <object name> [speed]\n\nwhere "object name" is the sub-topic directly under lasr_object_controller'
    else:
        rospy.init_node('lasr_key_teleop')
        
        # validate speed option
        if len(sys.argv) == 3:
            try:
                curses.wrapper(main, sys.argv[1], int(sys.argv[2]))
            except ValueError:
                print 'Invalid option: speed must be an integer'
        else:
            curses.wrapper(main, sys.argv[1], 1)